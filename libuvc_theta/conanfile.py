import os
from conans import ConanFile, CMake, tools

class UrfExternalsLibUvcTheta(ConanFile):
    name = "libuvc_theta"
    version = "1.0.0"
    license = "MIT"
    url = "https://github.com/ricohapi/libuvc-theta"
    description = "libuvc wrapper for Ricoh Theta V/Z1 camera"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False }
    requires = ("libusb/1.0.24")
    generators = "cmake", "cmake_find_package", "virtualenv"
    
    @property
    def _is_nvidia_jetson(self):
        if tools.os_info.is_linux:
            try:
                with open('/proc/device-tree/model') as f:
                    jetson_file = f.read()
                    if "jetson" in jetson_file.lower():
                        return True
            except IOError:
                pass
        return False

    def requirements(self):
        if not self._is_nvidia_jetson:
            self.requires("libjpeg-turbo/2.1.4")

    def source(self):
        self.run("git clone --recursive https://github.com/ricohapi/libuvc-theta")

        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "find_package(LibUSB)", "find_package(libusb)")
        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "find_package(JpegPkg QUIET)", "find_package(libjpeg-turbo)")
        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "if(JPEG_FOUND)", "if(libjpeg-turbo_FOUND)")
        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "LibUSB::LibUSB", "${libusb_LIBRARIES}")
        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "JPEG::JPEG", "${libjpeg-turbo_LIBRARIES}")
        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "$<INSTALL_INTERFACE:include>", "$<INSTALL_INTERFACE:include> ${libjpeg-turbo_INCLUDE_DIRS} ${libusb_INCLUDE_DIRS}")
        tools.replace_in_file("libuvc-theta/CMakeLists.txt", "include/libuvc/", "include/libuvc-theta/")

        os.rename("libuvc-theta/include/libuvc", "libuvc-theta/include/libuvc-theta")
        for filename in os.listdir("libuvc-theta/include/libuvc-theta"):
            try:
                tools.replace_in_file("libuvc-theta/include/libuvc-theta/"+filename, "uvc_", "uvc_theta_")
                tools.replace_in_file("libuvc-theta/include/libuvc-theta/"+filename, "libuvc_theta_", "libuvc_")
                tools.replace_in_file("libuvc-theta/include/libuvc-theta/"+filename, "libuvc/", "libuvc-theta/")
            except:
                pass

        for filename in os.listdir("libuvc-theta/src/"):
            try:
                tools.replace_in_file("libuvc-theta/src/"+filename, "uvc_", "uvc_theta_")
                tools.replace_in_file("libuvc-theta/src/"+filename, "libuvc_theta_", "libuvc_")
                tools.replace_in_file("libuvc-theta/src/"+filename, "libuvc/", "libuvc-theta/")
            except:
                pass


    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        if self.options.shared:
            cmake.definitions["CMAKE_BUILD_TARGET"] = "Shared"
        else:
            cmake.definitions["CMAKE_BUILD_TARGET"] = "Static"

        cmake.definitions["BUILD_EXAMPLE"] = False
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = not self.options.shared

        cmake.configure(
            source_folder="libuvc-theta",
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst="include/libuvc-theta", src=os.path.join(self.recipe_folder, 'package/include/libuvc'))
        if tools.os_info.is_linux:
            if self.options.shared:
                os.rename(os.path.join(self.recipe_folder, 'package/lib/libuvc.so'), os.path.join(self.recipe_folder, 'package/lib/libuvc_theta.so'))
            else:
                os.rename(os.path.join(self.recipe_folder, 'package/lib/libuvc.a'), os.path.join(self.recipe_folder, 'package/lib/libuvc_theta.a'))

        self.copy("*.a", dst="lib" , src=os.path.join(self.recipe_folder, 'package/lib'))
        self.copy("*.so", dst="lib" , src=os.path.join(self.recipe_folder, 'package/lib'))

    def package_info(self):
        self.cpp_info.libs = ['uvc_theta']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib/']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))
