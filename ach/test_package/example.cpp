#include <iostream>
#include <ach.h>

int main() {
    auto r = ach_create( "my_channel", 10, 512, NULL );
    if( ACH_OK != r ) {
        fprintf( stderr, "Could not create channel: %s\n", ach_result_to_string(r) );
        exit(EXIT_FAILURE);
    }

    r = ach_unlink("my_channel");

    if( ACH_OK != r ) {
        fprintf( stderr, "Could not create channel: %s\n", ach_result_to_string(r) );
        exit(EXIT_FAILURE);
    }

    std::cout << "All good" << std::endl;

    return 0;
}
