import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools

class UrfExternalsAch(ConanFile):
    name = "ach"
    version = "2.0.0"
    license = "BSD-3"
    url = "https://github.com/golems/ach"
    description = "Ach is an Inter-Process Communication (IPC) mechanism and library. It is especially suited for communication in real-time systems that sample data from physical processes."
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "kernel_module": [True, False]
    }
    default_options = {"shared": True, "kernel_module": False}
    requires = ()
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run("git clone --recursive https://github.com/golems/ach.git")

        tools.replace_in_file("ach/src/libach_posix.c", "chan->next_index = 1;", "chan->next_index = 0;")
        tools.replace_in_file("ach/include/ach.h", "#include <signal.h>", "#include <signal.h>\n#include <stdlib.h>\n#include <stdint.h>\n#include <time.h>\n")

    def requirements(self):
        if not tools.os_info.is_linux:
            print("This library is available only in Linux")
            return False

        if tools.os_info.is_linux and tools.os_info.linux_distro in ("ubuntu"):
            self.__ubuntu_requirements()

    def build(self):
        kernel_module = "no"
        if self.options.kernel_module:
            kernel_module = "yes"

        self.run("cd ach && autoreconf -i && autoconf")
        self.run("cd ach && ./configure --disable-dependency-tracking --prefix=" + os.path.join(self.recipe_folder, 'package/') + " --enable-kbuild=no --enable-dkms="+kernel_module+" --enable-dkms-build="+kernel_module)
        self.run("cd ach && make && make install")

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))

    def package_info(self):
        self.cpp_info.libs = ['ach', 'ach-experimental']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))

    def __ubuntu_requirements(self):
        installer = tools.SystemPackageTool()
        installer.update()

        if not installer.installed("pkg-config"):
            installer.install("pkg-config")

        if not installer.installed("autoconf"):
            installer.install("autoconf")

        if not installer.installed("automake"):
            installer.install("automake")

        if not installer.installed("libtool"):
            installer.install("libtool")

        if not installer.installed("autoconf-archive"):
            installer.install("autoconf-archive")

        if not installer.installed("doxygen"):
            installer.install("doxygen")

        if not installer.installed("openbsd-inetd"):
                installer.install("openbsd-inetd")

        if not installer.installed("avahi-utils"):
                installer.install("avahi-utils")

        if self.options.kernel_module:
            if not installer.installed("dkms"):
                installer.install("dkms")