import os
from conans import ConanFile, CMake, tools

class UrfExternalsPicoQuic(ConanFile):
    name = "picoquic"
    version = "1.0.0"
    license = "MIT"
    url = "https://github.com/private-octopus/picoquic"
    description = "Minimalist implementation of the QUIC protocol, as defined by the IETF."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    requires = ("picotls/1.0.0@urobf+externals+picotls/stable", "openssl/1.1.1k")
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run("git clone --recursive https://github.com/private-octopus/picoquic")

    # def requirements(self):
    #     if tools.os_info.is_linux and tools.os_info.linux_distro in ("ubuntu"):
    #         self.__ubuntu_requirements()

    def configure(self):
        self.options["picotls"].shared = self.options.shared

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = not self.options.shared

        picotls_libs_extension = None
        openssl_libs_extension = None
        libs_prepend = None
        if tools.os_info.is_linux:
            libs_prepend = 'lib'
            picotls_libs_extension = '.a'
            if self.options['picotls'].shared:
                picotls_libs_extension = '.so'

            openssl_libs_extension = '.a'
            if self.options['openssl'].shared:
                openssl_libs_extension = '.so'
        
        cmake.definitions["PTLS_CORE_LIBRARY"] = os.path.join(self.deps_cpp_info['picotls'].lib_paths[0], libs_prepend + 'picotls-core' + picotls_libs_extension)
        cmake.definitions["PTLS_OPENSSL_LIBRARY"] = os.path.join(self.deps_cpp_info['picotls'].lib_paths[0], libs_prepend + 'picotls-openssl' + picotls_libs_extension)
        cmake.definitions["PTLS_FUSION_LIBRARY"] = os.path.join(self.deps_cpp_info['picotls'].lib_paths[0], libs_prepend + 'picotls-fusion' + picotls_libs_extension)
        cmake.definitions["PTLS_INCLUDE_DIR"] = self.deps_cpp_info['picotls'].include_paths[0]

        cmake.definitions["OPENSSL_INCLUDE_DIR"] = self.deps_cpp_info['openssl'].include_paths[0]
        cmake.definitions["OPENSSL_SSL_LIBRARY"] = os.path.join(self.deps_cpp_info['openssl'].lib_paths[0], libs_prepend + 'crypto' + openssl_libs_extension)
        cmake.definitions["OPENSSL_CRYPTO_LIBRARY"] = os.path.join(self.deps_cpp_info['openssl'].lib_paths[0], libs_prepend + 'ssl' + openssl_libs_extension)
    
        cmake.configure(
            source_folder="picoquic",
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))

    def package_info(self):
        self.cpp_info.libs = ['picohttp-core', 'picoquic-core', 'picoquic-log']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))


    def __ubuntu_requirements(self):
        installer = tools.SystemPackageTool()
        if not installer.installed("pkg-config"):
            installer.update()
            installer.install("pkg-config")
