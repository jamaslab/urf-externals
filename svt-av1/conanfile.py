import os
from conans import ConanFile, CMake, tools

class UrfExternalsSvtAv1(ConanFile):
    name = "svt-av1"
    license = "MIT"
    version = "1.3.0"
    url = "https://gitlab.com/AOMediaCodec/SVT-AV1"
    description = "Scalable Video Technology for AV1 (SVT-AV1 Encoder and Decoder)"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    build_requires = ("yasm/1.3.0")
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run("git clone --depth=1 https://gitlab.com/AOMediaCodec/SVT-AV1.git")
        self.run(f"cd SVT-AV1 && git checkout v{self.version}")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = not self.options.shared
    
        cmake.configure(
            source_folder="SVT-AV1",
            build_folder='build_artifacts/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))

    def package_info(self):
        self.cpp_info.libs = ['SvtAv1Dec', 'SvtAv1Enc']
