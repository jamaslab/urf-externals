from conans import ConanFile, AutoToolsBuildEnvironment, tools
import glob
import os
import shutil


class UrfExternalsRpLidar(ConanFile):
    name = "rplidar_sdk"
    version = "2.0.0"
    license = "MIT"
    url = "https://github.com/slamtec/rplidar_sdk"
    description = "Slamtec RPLIDAR Public SDK for C++"
    settings = "os", "compiler", "build_type", "arch"
    short_paths = True
    build_requires = ("cmake/3.25.0")
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run(
            "git clone --recursive https://github.com/slamtec/rplidar_sdk")
    def configure(self):
        if self.settings.os != "Linux":
            raise Exception("Only Linux is supported")
    
    def build_requirements(self):
        pass

    def build(self):
        if self.settings.build_type == "Debug":
            self.run("cd rplidar_sdk && make DEBUG=1")
        else:
            self.run("cd rplidar_sdk && make")

    def package(self):
        self.copy("*.h", dst="include/rplidar_sdk", src=os.path.join(self.build_folder,
                  'rplidar_sdk/', 'sdk/', 'include/'), keep_path=False)
        self.copy("*.h", dst="include/rplidar_sdk", src=os.path.join(self.build_folder,
                  'rplidar_sdk/', 'sdk/', 'src/'), keep_path=True)

        self._copy_recursive(os.path.join(self.build_folder, 'rplidar_sdk',
                                          'output'), ext='*.a', dst="lib/")
        self._copy_recursive(os.path.join(self.build_folder, 'rplidar_sdk',
                                          'output'), ext='*.so', dst="lib/")

    def package_info(self):
        self.cpp_info.libs = ['sl_lidar_sdk']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))

    def _copy_recursive(self, path, ext, dst):
        if not os.path.exists(os.path.join(self.package_folder, dst)):
            os.makedirs(os.path.join(self.package_folder, dst))

        for file_path in glob.glob(os.path.join(path, '**', ext), recursive=True):
            new_path = os.path.join(os.path.join(
                self.package_folder, dst), os.path.basename(file_path))
            shutil.copy(file_path, new_path)
