import os
from conans import ConanFile, CMake, tools

class UrfExternalsFBow(ConanFile):
    name = "fbow"
    version = "1.0.0"
    license = "MIT"
    url = "https://github.com/stella-cv/FBoW"
    description = "g2o - General Graph Optimization"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False }
    requires = ("opencv/4.5.0")
    build_requires = ()
    generators = "cmake", "cmake_find_package", "virtualenv"

    @property
    def _is_nvidia_jetson(self):
        if tools.os_info.is_linux:
            try:
                with open('/proc/device-tree/model') as f:
                    jetson_file = f.read()
                    if "jetson" in jetson_file.lower():
                        return True
            except IOError:
                pass
        return False

    def configure(self):
        self.options["opencv"].with_jpeg = "libjpeg-turbo"
        self.options["opencv"].with_ffmpeg = False

        if self._is_nvidia_jetson:
            self.options["opencv"].with_jpeg = False
            self.options["opencv"].with_tiff = False
            self.options["opencv"].with_gtk = False
            self.options["opencv"].with_jpeg2000 = False

    def source(self):
        self.run("git clone --recursive https://github.com/stella-cv/FBoW")

        tools.replace_in_file("FBoW/CMakeLists.txt", "include_directories(${OpenCV_INCLUDE_DIRS})", "include_directories(${opencv_INCLUDE_DIRS})")
        tools.replace_in_file("FBoW/CMakeLists.txt", "set(REQUIRED_LIBRARIES \"${OpenCV_LIBS}\")", "set(REQUIRED_LIBRARIES \"${opencv_LIBS}\")")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type

        cmake.definitions["BUILD_TESTS"] = False
        cmake.definitions["BUILD_UTILS"] = False

        cmake.configure(
            source_folder="FBoW",
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))

    def package_info(self):
        self.cpp_info.libs = ['fbow']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib/']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))
