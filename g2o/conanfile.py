import os
from conans import ConanFile, CMake, tools

class UrfExternalsG2o(ConanFile):
    name = "g2o"
    version = "1.0.0"
    license = "MIT"
    url = "https://github.com/ricohapi/libuvc-theta"
    description = "g2o - General Graph Optimization"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False }
    requires = ("eigen/3.3.9")
    build_requires = ("cmake/3.25.0")
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run("git clone --recursive https://github.com/RainerKuemmerle/g2o.git")

    def requirements(self):
        installer = tools.SystemPackageTool()
        if tools.os_info.is_linux:
            installer = tools.SystemPackageTool()
            library_name = ""
            if tools.os_info.linux_distro in ("ubuntu"):
                library_name = "libsuitesparse-dev"

            if not installer.installed(library_name):
                try:
                    installer.install(library_name, update=True)
                except:
                    print("Could not install libsuitesparse")


    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared

        cmake.definitions["BUILD_UNITTESTS"] = False
        cmake.definitions["G2O_BUILD_EXAMPLES"] = False
        cmake.definitions["G2O_BUILD_PLAYGROUND"] = False
        cmake.definitions["G2O_BUILD_APPS"] = False
        cmake.definitions["G2O_BUILD_BENCHMARKS"] = False

        cmake.configure(
            source_folder="g2o",
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))

    def package_info(self):
        self.cpp_info.libs = ['g2o_core', 'g2o_solver_dense', 'g2o_solver_eigen', 'g2o_solver_pcg', 'g2o_solver_slam2d_linear', 'g2o_solver_structure_only', 'g2o_stuff', 'g2o_types_data', 'g2o_types_icp', 'g2o_types_sba', 'g2o_types_sclam2d', 'g2o_types_sim3', 'g2o_types_slam2d_addons', 'g2o_types_slam2d', 'g2o_types_slam3d_addons', 'g2o_types_slam3d']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib/']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))
