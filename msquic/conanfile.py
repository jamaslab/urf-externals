import os
from conans import ConanFile, CMake, tools


class UrfExternalsMsQuic(ConanFile):
    name = "msquic"
    version = "2.0.3"
    license = "MIT"
    url = "https://github.com/microsoft/msquic"
    description = "MsQuic is a Microsoft implementation of the IETF QUIC protocol. It is cross-platform, written in C and designed to be a general purpose QUIC library."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    build_requires = ("cmake/3.21.3")
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run(
            "git clone --recursive --branch v{} https://github.com/microsoft/msquic.git".format(self.version))

    def build_requirements(self):
        if self.settings.os == "Windows":
            self.build_requires("nasm/2.15.05")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(
            self.source_folder, 'package/')
        # cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace(
        #     "\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["QUIC_OUTPUT_DIR"] = os.path.join(
            self.source_folder, f"build/bin/{cmake.definitions['CMAKE_BUILD_TYPE']}")
                
        cmake.definitions["QUIC_BUILD_TOOLS"] = False
        cmake.definitions["QUIC_BUILD_TEST"] = False
        cmake.definitions["QUIC_BUILD_PERF"] = False
        cmake.definitions["QUIC_BUILD_SHARED"] = self.options.shared
        cmake.definitions["QUIC_OPTIMIZE_LOCAL"] = True
        cmake.definitions["QUIC_PGO"] = True
        cmake.definitions["QUIC_USE_SYSTEM_LIBCRYPTO"] = True
        cmake.definitions["QUIC_HIGH_RES_TIMERS"] = True

        if self.settings.os == "Windows" and self.settings.compiler == "Visual Studio" and int(str(self.settings.compiler.version)) < 16:
            tools.replace_in_file("msquic/CMakeLists.txt", "set(QUIC_WARNING_FLAGS /WX /W4 /sdl CACHE INTERNAL \"\")", "set(QUIC_WARNING_FLAGS /W4 /sdl CACHE INTERNAL \"\")")

        if self.settings.os == "Windows":
            cmake.definitions["QUIC_TLS"] = "openssl"

        cmake.configure(
            source_folder="msquic",
            build_folder="build/")

        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*.h", dst="include/", src=os.path.join(self.source_folder, 'package/'), keep_path=False)
        self.copy("*.so", dst="lib/", src=os.path.join(self.build_folder,
                                                      'build'), keep_path=False)
        self.copy("*.a", dst="lib/", src=os.path.join(self.build_folder,
                                                      'build'), keep_path=False)

        self.copy("*.lib", dst="lib/", src=os.path.join(self.build_folder,
                                                      'build'), keep_path=False)
        self.copy("*.dll", dst="lib/", src=os.path.join(self.build_folder,
                                                      'build'), keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ['core', 'platform']
        if not self.options.shared:
            self.cpp_info.libs.append('msquic_static')
        else:
            self.cpp_info.libs.append('msquic')

        if tools.os_info.is_linux:
            self.cpp_info.libs.append('apps')
            self.cpp_info.libs.append('ssl')
            self.cpp_info.libs.append('crypto')
            self.cpp_info.libs.append('dl')

        if tools.os_info.is_windows:
            self.cpp_info.libs.append('libapps')
            self.cpp_info.libs.append('libssl')
            self.cpp_info.libs.append('libcrypto')
            self.cpp_info.libs.append('MsQuicEtw_Resource')
            self.cpp_info.libs.append('ncrypt')
            self.cpp_info.libs.append('Iphlpapi')
            self.cpp_info.libs.append('Crypt32')
            self.cpp_info.libs.append('ntdll')
            self.cpp_info.libs.append('Winmm')

        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))
