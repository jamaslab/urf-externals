import os
from conans import ConanFile, CMake, tools

class UrfExternalsLibUvcTheta(ConanFile):
    name = "pangolin"
    version = "0.6.0"
    license = "MIT"
    url = "https://github.com/ricohapi/libuvc-theta"
    description = "libuvc wrapper for Ricoh Theta V/Z1 camera"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False }
    requires = ("glew/2.2.0", "eigen/3.4.0")
    build_requires = "cmake/3.22.0"
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run("git clone -b v0.6 --recursive https://github.com/stevenlovegrove/Pangolin.git && mv Pangolin source")

    def configure(self):
        info = tools.OSInfo()
        if info.is_linux:
            installer = tools.SystemPackageTool()
            installer.update()
            if info.linux_distro in ("debian", "ubuntu"):
                installer.install("libgl1-mesa-dev")
                installer.install("libglu1-mesa-dev")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_TESTS"] = False
        cmake.definitions["BUILD_TOOLS"] = False
        cmake.definitions["BUILD_EXAMPLES"] = False
        cmake.definitions["BUILD_PANGOLIN_PYTHON"] = False
        cmake.definitions["BUILD_PANGOLIN_V4L"] = False
        cmake.definitions["BUILD_PANGOLIN_FFMPEG"] = False
        cmake.definitions["BUILD_PANGOLIN_OPENNI"] = False
        cmake.definitions["BUILD_PANGOLIN_OPENNI2"] = False
        cmake.definitions["BUILD_PYPANGOLIN_MODULE"] = False
        cmake.definitions["BUILD_PANGOLIN_LIBREALSENSE"] = False
        cmake.definitions["BUILD_PANGOLIN_LIBREALSENSE2"] = False
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = not self.options.shared

        cmake.configure(
            source_folder="source",
            build_folder="build")
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))


    def package_info(self):
        self.cpp_info.libs = ['pangolin']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib/']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))
