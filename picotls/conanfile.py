import os
from conans import ConanFile, CMake, tools

class UrfExternalsPicoTls(ConanFile):
    name = "picotls"
    version = "1.0.0"
    license = "MIT"
    url = "https://github.com/h2o/picotls"
    description = "Picotls is a TLS 1.3 (RFC 8446) protocol stack written in C."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True}
    requires = ("openssl/1.1.1k")
    generators = "cmake", "cmake_find_package", "virtualenv"

    def source(self):
        self.run("git clone --recursive https://github.com/h2o/picotls")

        with open("picotls/CMakeLists.txt", "a") as cmakeFile:
            cmakeFile.write('''install(TARGETS picotls-core EXPORT picotls-core DESTINATION lib/)
                install(TARGETS picotls-minicrypto EXPORT picotls-minicrypto DESTINATION lib/)

                IF (OPENSSL_FOUND AND NOT (OPENSSL_VERSION VERSION_LESS "1.0.1"))
                    install(TARGETS picotls-openssl EXPORT picotls-openssl DESTINATION lib/)
                endif()

                IF ((CMAKE_SIZEOF_VOID_P EQUAL 8) AND
                    (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64") OR
                    (CMAKE_SYSTEM_PROCESSOR STREQUAL "amd64") OR
                    (CMAKE_SYSTEM_PROCESSOR STREQUAL "AMD64"))

                    install(TARGETS picotls-fusion EXPORT picotls-fusion DESTINATION lib/)
                endif()
                install(DIRECTORY include/ DESTINATION include FILES_MATCHING PATTERN "*.h")
            ''')


    def requirements(self):
        if tools.os_info.is_linux and tools.os_info.linux_distro in ("ubuntu"):
            self.__ubuntu_requirements()

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = not self.options.shared

        cmake.configure(
            source_folder="picotls",
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", dst=".", src=os.path.join(self.recipe_folder, 'package/'))

    def package_info(self):
        self.cpp_info.libs = ['picotls-core', 'picotls-fusion', 'picotls-minicrypto', 'picotls-openssl']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))

    def __ubuntu_requirements(self):
        installer = tools.SystemPackageTool()
        if not installer.installed("pkg-config"):
            installer.update()
            installer.install("pkg-config")
